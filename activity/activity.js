db.users.insertMany([
    {
        "name": "Justin",
        "lastName": "Timberlake",
        "email": "leadsinger@gmail.com",
        "password": "leadsinger",
        "isAdmin": false
    
    },
    {
        "name": "JC",
        "lastName": "Chasez",
        "email": "wannabe@gmail.com",
        "password": "wannabelead",
        "isAdmin": false
        
    },
    {
        "name": "Lance",
        "lastName": "Bass",
        "email": "blondeone@gmail.com",
        "password": "blonde",
        "isAdmin": false
        
    },
    {
        "name": "Joey",
        "lastName": "Fatone",
        "email": "italianmb@gmail.com",
        "password": "meatball",
        "isAdmin": false

    },
    {
        "name": "Chris",
        "lastName": "Kirkpatrick",
        "email": "lastone@gmail.com",
        "password": "falsetto",
        "isAdmin": false

    },

])


db.courses.insertMany([

    {
        "name": "Singing",
        "price": 1000,
        "isActive": false
    },
    {
        "name": "Dancing",
        "price": 1000,
        "isActive": false
    },
    {
        "name": "Acrobatics",
        "price": 2000,
        "isActive": false
    }
    
])


db.users.find({"isAdmin":false})


db.users.updateOne({},{$set:{"isAdmin":true}})


db.courses.updateOne({},{$set:{"isActive":true}})


db.courses.deleteMany({"isActive":false})



